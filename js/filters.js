(function () {

  'use strict';

  angular.module('pullRequestNotifier.filters', [])

    .filter('translate', function () {
      return function (key, replacements) {
        if (Array.isArray(replacements)) {
          return chrome.i18n.getMessage(key, replacements);
        } else if (replacements !== undefined) {
          return chrome.i18n.getMessage(key, [replacements]);
        } else {
          return chrome.i18n.getMessage(key);
        }
      };
    });

})();