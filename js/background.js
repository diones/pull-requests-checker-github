(function () {

  'use strict';

  angular.module('pullRequestNotifierBg', ['pullRequestNotifier.services', 'pullRequestNotifier.filters'])
    .config(['$logProvider', function ($logProvider) {
      $logProvider.debugEnabled(false);
    }])

    .run(['$log', '$q', '$window', 'ConfigService', 'GithubApiService', 'NotificationService', function ($log, $q, $window, ConfigService, GithubApiService, NotificationService) {

      var interval;
      var checkPullRequests = function () {
        _gaq.push(['_trackPageview']);

        ConfigService.load().then(function (options) {
          if (options.reposObj.length > 0) {
            $log.debug('Checking pull requests...');

            var promises = [];

            angular.forEach(options.reposObj, function (repo) {
              var promise = GithubApiService.listPullRequests(repo.owner, repo.repo, options.token).then(function (pullRequestRepo) {
                return pullRequestRepo.pullRequests.length;
              }, function () {
                return 0;
              });
              promises.push(promise);
            });

            $q.all(promises).then(function (pullRequestsNums) {
              var pullRequestsSum = 0;
              pullRequestsNums.forEach(function (value) {
                pullRequestsSum += value;
              });

              NotificationService.updateBadge(pullRequestsSum);

              // chrome.browserAction.setTitle({
              //   title: chrome.i18n.getMessage("title")
              // });

            });
            interval = $window.setTimeout(checkPullRequests, options.interval * 1000 * 60);
          } else {
            NotificationService.updateBadge("");
          }
        });
      };

      checkPullRequests();

      chrome.alarms.onAlarm.addListener(function (alarm) {
        if (alarm.name === 'options_updated') {
          ConfigService.load().then(function (options) {
            if (interval !== undefined) {
              $window.clearInterval(interval);
            }
            checkPullRequests();
          });
        }
      });

      $window.onunload = function () {
        if (interval !== undefined) {
          $window.clearInterval(interval);
        }
      };

    }]);

})();