(function () {

  'use strict';

  angular.module('pullRequestNotifier.controllers', [])

    .controller('PopupCtrl', ['$scope', '$q', '$log', '$mdToast', '$filter', 'ConfigService', 'GithubApiService', 'NotificationService', function ($scope, $q, $log, $mdToast, $filter, ConfigService, GithubApiService, NotificationService) {

      _gaq.push(['_trackPageview']);

      var isRepositoryCandidate = false;

      chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
        $scope.activeTabUrl = tabs[0].url;
        $log.debug('url: ' + $scope.activeTabUrl);
        var regExp = /github\.com\/[a-zA-Z-_]+\/[a-zA-Z-_]+/;

        if (regExp.test($scope.activeTabUrl)) {
          $scope.repositoryToAdd = $scope.activeTabUrl.match(regExp)[0].replace('github.com/', '');
          isRepositoryCandidate = true;
        }
      });

      ConfigService.load().then(function (options) {

        var promises = [];

        angular.forEach(options.reposObj, function (repo) {
          var promise = GithubApiService.listPullRequests(repo.owner, repo.repo, options.token).then(function (pullRequestRepo) {
            return pullRequestRepo;
          }, function () {
            return;
          });
          promises.push(promise);
        });

        if (promises.length > 0) {
          $q.all(promises).then(function (pullRequestRepos) {
            $scope.pullRequestRepos = pullRequestRepos;
            var pullRequestsSum = 0;
            pullRequestRepos.forEach(function (pullRequestsRepo) {
              pullRequestsSum += pullRequestsRepo.pullRequests.length;
            });

            NotificationService.updateBadge(pullRequestsSum);
          });
        } else {
          $scope.pullRequestRepos = [];
        }

        if (isRepositoryCandidate && options.repos.indexOf($scope.repositoryToAdd) === -1) {
          $scope.showAddRepository = true;
        }
      });

      $scope.addRepository = function () {
        _gaq.push(['_trackEvent', 'Repository', 'Added: ' + $scope.repositoryToAdd]);
        ConfigService.addRepository($scope.repositoryToAdd).then(function (options) {
          chrome.alarms.create('options_updated', { when: Date.now() });
          $scope.showAddRepository = false;
          $mdToast.show(
            $mdToast.simple()
              .textContent($filter('translate')('repositoryAdded', $scope.repositoryToAdd))
              .hideDelay(3000)
          );

          var promises = [];
          angular.forEach(options.reposObj, function (repo) {
            var promise = GithubApiService.listPullRequests(repo.owner, repo.repo, options.token).then(function (pullRequestRepo) {
              return pullRequestRepo;
            }, function () {
              return;
            });
            promises.push(promise);
          });

          if (promises.length > 0) {
            $q.all(promises).then(function (pullRequestRepos) {
              $scope.pullRequestRepos = pullRequestRepos;
            });
          }
        });
      };

      $scope.openOptionsPage = function () {
        chrome.tabs.create({ 'url': 'chrome://extensions/?options=' + chrome.runtime.id });
      };

      chrome.alarms.onAlarm.addListener(function (alarm) {
        if (alarm.name === 'options_updated') {
          ConfigService.load().then(function (options) {
            angular.forEach(options.reposObj, function (repo) {
              var promise = GithubApiService.listPullRequests(repo.owner, repo.repo, options.token).then(function (pullRequestRepo) {
                return pullRequestRepo;
              }, function () {
                return;
              });
              promises.push(promise);
            });

            $q.all(promises).then(function (pullRequestRepos) {
              $scope.pullRequestRepos = pullRequestRepos;
            });
          });
        }
      });

    }])

    .controller('OptionsCtrl', ['$log', '$scope', '$mdToast', '$filter', 'ConfigService', function ($log, $scope, $mdToast, $filter, ConfigService) {

      _gaq.push(['_trackPageview']);

      ConfigService.load().then(function (options) {
        $scope.options = options;
      });

      $scope.save = function (options) {
        ConfigService.save(options).then(function (options) {
          chrome.alarms.create('options_updated', { when: Date.now() });
          $mdToast.show(
            $mdToast.simple()
              .textContent($filter('translate')('optionsSaved'))
              .hideDelay(3000)
          );
        });
      };

    }]);

})();