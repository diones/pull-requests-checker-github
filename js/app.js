(function () {

  'use strict';

  angular.module('pullRequestNotifier', ['pullRequestNotifier.controllers', 'pullRequestNotifier.services', 'pullRequestNotifier.filters', 'ngMaterial', 'ngSanitize', 'tmh.dynamicLocale'])

    .run(['$log', '$window', 'tmhDynamicLocale', function ($log, $window, tmhDynamicLocale) {
      var lang = $window.navigator.language || $window.navigator.userLanguage;
      $log.debug(lang);
      if (lang.match(/^en/i) === null) {
        tmhDynamicLocale.set(lang.toLowerCase()).then(function () {
          $log.debug('angular locale file found! - new language loaded.');
        }, function () {
          $log.debug('angular locale file not found! - using default language.');
        });
      } else {
        $log.debug('angular locale file is not needed!');
      }
    }])

    .config(['$logProvider', '$compileProvider', '$mdThemingProvider', 'tmhDynamicLocaleProvider', function ($logProvider, $compileProvider, $mdThemingProvider, tmhDynamicLocaleProvider) {
      $logProvider.debugEnabled(false);

      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);

      $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('orange');

      $mdThemingProvider.theme('teal').backgroundPalette('teal').dark();

      tmhDynamicLocaleProvider.localeLocationPattern('js/i18n/angular-locale_{{locale}}.js');
    }]);
})();