(function () {

  'use strict';

  angular.module('pullRequestNotifier.services', [])

    .factory('ConfigService', ['$q', function ($q) {

      var makeRepoObj = function (repository) {
        var split;
        var owner;
        var repo;
        split = repository.split('/');
        owner = Array.isArray(split) ? split[0] : split;
        repo = Array.isArray(split) && split.length > 0 ? split[1] : "";
        return { owner: owner, repo: repo };
      };

      var makeRepoObjs = function (repos) {
        var reposObj = [];
        for (var i = 0; i < repos.length; i++) {
          reposObj.push(makeRepoObj(repos[i]));
        }
        return reposObj;
      };

      return {
        load: function () {
          return $q(function (resolve) {
            chrome.storage.sync.get({
              token: '',
              interval: 10,
              repos: [],
              reposObj: []
            }, function (options) {
              resolve(options);
            });
          });
        },
        addRepository: function (repository) {
          var _this = this;
          return $q(function (resolve) {
            _this.load().then(function (options) {
              options.repos.push(repository);
              options.reposObj.push(makeRepoObj(repository));

              chrome.storage.sync.set({
                repos: options.repos,
                reposObj: options.reposObj
              }, function () {
                resolve(options);
              });
            });
          });
        },
        save: function (options) {
          return $q(function (resolve) {
            var repos = [];
            var reposObj = [];
            if (Array.isArray(options.repos)) {
              repos = options.repos;
              reposObj = makeRepoObjs(options.repos);
            }

            chrome.storage.sync.set({
              token: options.token,
              interval: options.interval,
              repos: repos,
              reposObj: reposObj
            }, function () {
              resolve(options);
            });
          });
        }
      };
    }])

    .factory('GithubApiService', ['$http', '$log', function ($http, $log) {

      var apiUrl = 'https://api.github.com/';
      var mainUrl = 'https://github.com/';

      return {
        listPullRequests: function (owner, repo, token) {
          var headers = {
            Accept: 'application/vnd.github.v3+json'
          };
          if (angular.isDefined(token) && token.trim().length > 0) {
            angular.extend(headers, {
              Authorization: 'token ' + token
            });
          }

          var url = apiUrl + 'repos/' + owner + '/' + repo + '/pulls';
          $log.debug("Requesting -> " + url);

          return $http({
            method: 'GET',
            url: url,
            headers: headers
          }).then(function successCallback(response) {
            var pullRequestRepo = {
              owner: owner,
              repo: repo,
              fullName: owner + '/' + repo,
              link: mainUrl + owner + '/' + repo,
              pullRequests: response.data,
              notFound: false,
              unauthorized: false,
              forbidden: false
            };
            $log.debug(pullRequestRepo);
            return pullRequestRepo;
          }, function errorCallback(response) {
            var pullRequestRepo = {
              owner: owner,
              repo: repo,
              fullName: owner + '/' + repo,
              link: "#",
              pullRequests: [],
              notFound: response.status === 404,
              unauthorized: response.status === 401,
              forbidden: response.status === 403
            };
            $log.error(response.data);
            return pullRequestRepo;
          });
        }
      };
    }])

    .factory('NotificationService', ['$log', function ($log) {
      return {
        updateBadge: function (pullRequestsNum) {
          chrome.browserAction.setBadgeText({
            text: '' + pullRequestsNum
          });
        }
      };
    }]);

})();