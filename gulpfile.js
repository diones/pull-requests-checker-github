var gulp = require('gulp');
var clean = require('gulp-clean');
var zip = require('gulp-zip');

gulp.task('copy_libs', ['clean'], function () {
    return gulp.src(['./libs/**/*.min.css', './libs/**/*.min.js'])
        .pipe(gulp.dest('./dist/libs/'));
});

gulp.task('copy_locales', ['clean'], function () {
    return gulp.src('./_locales/**/*.json')
        .pipe(gulp.dest('./dist/_locales'));
});

gulp.task('copy_extension_files', ['clean'], function () {
    return gulp.src('./manifest.json')
        .pipe(gulp.dest('./dist/'));
});

gulp.task('copy_images', ['clean'], function () {
    return gulp.src(['./images/**/*.svg', './images/**/*.png'])
        .pipe(gulp.dest('./dist/images/'));
});

gulp.task('copy_html', ['clean'], function () {
    return gulp.src('./pages/**/*.html')
        .pipe(gulp.dest('./dist/pages/'));
});

gulp.task('copy_js', ['clean'], function () {
    return gulp.src('./js/**/*.js')
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('copy_css', ['clean'], function () {
    return gulp.src('./css/**/*.css')
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('clean', function () {
    return gulp.src('./dist', { read: false })
        .pipe(clean());
});

gulp.task('build', ['copy_images', 'copy_html', 'copy_js', 'copy_css', 'copy_extension_files', 'copy_libs', 'copy_locales']);

gulp.task('zip', ['build'], function () {
    return gulp.src('./dist/**/*')
        .pipe(zip('pull_request_checker.zip'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['clean', 'build', 'zip']);